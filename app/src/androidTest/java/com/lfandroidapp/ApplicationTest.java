package com.lfandroidapp;

import android.app.Application;
import android.test.ApplicationTestCase;

import com.parse.Parse;
import com.parse.ParseObject;

/**
 * <a href="http://d.android.com/tools/testing/testing_android.html">Testing Fundamentals</a>
 */
public class ApplicationTest extends ApplicationTestCase<Application> {
    public ApplicationTest() {
        super(Application.class);

        Parse.enableLocalDatastore(this.getApplication());

        Parse.initialize(this.getApplication(), "aaPQgmEw8AGJ8YyUUnJpwXDt8wBglhhUAueWtK9t", "FDxqU9woxAolhcKGrmXPs37uaiHt3jhkfWpRcRRe");

        ParseObject testObject = new ParseObject("TestObject");
        testObject.put("foo", "bar");
        testObject.saveInBackground();
    }
}