package com.lfandroidapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseUser;

import butterknife.Bind;
import butterknife.ButterKnife;

public class LoginActivity extends AppCompatActivity {
    private static final String TAG = "LoginActivity";
    private static final int REQUEST_SIGNUP = 0;

    @Bind(R.id.input_username)
    EditText edtUsername;

    @Bind(R.id.input_password)
    EditText edtPasswor;

    @Bind(R.id.btn_login)
    Button btnLogin;

    @Bind(R.id.link_signup)
    TextView txtLink;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        btnLogin.setEnabled(true);


        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();
            }
        });

        txtLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), SignUpActivity.class);
                startActivityForResult(intent, REQUEST_SIGNUP);
            }
        });

    }

    private void login(){
        Log.d(TAG, "Login");

        if(!validate()){
            onLoginFailed();
            return;
        }
        btnLogin.setEnabled(false);

        final MaterialDialog progressDialog = new MaterialDialog.Builder(this)
                .content("Authenticating..")
                .progress(true, 0)
                .progressIndeterminateStyle(true)
                .show();

        new android.os.Handler().postDelayed(
                new Runnable() {
                    @Override
                    public void run() {
                        onLoginSuccess();
                        progressDialog.dismiss();
                    }
                },3000
        );
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == REQUEST_SIGNUP){
            if(resultCode == RESULT_OK){
                this.finish();
            }
        }
    }

    @Override
    public void onBackPressed() {
       moveTaskToBack(true);
    }

    private void onLoginSuccess(){
        String username = edtUsername.getText().toString();
        String password = edtPasswor.getText().toString();

        ParseUser.logInInBackground(username, password, new LogInCallback() {
                    @Override
                    public void done(ParseUser user, ParseException e) {
                        if(e == null){
                            if(user != null){
                                btnLogin.setEnabled(true);
                                finish();
                            }else{
                                onLoginFailed();
                            }
                        }else{
                            onLoginFailed();
                        }
                    }
                });

    }

    private void onLoginFailed(){
        Toast.makeText(getBaseContext(), "Login failed", Toast.LENGTH_LONG ).show();
        btnLogin.setEnabled(true);
    }

    private boolean validate(){
        boolean valid = true;

        String username = edtUsername.getText().toString();
        String password = edtPasswor.getText().toString();
        if(username.isEmpty() || username.length() < 5){
            edtUsername.setError("at least 5 character");
            valid = false;
        }else{
            edtUsername.setError(null);
        }

        if(password.isEmpty() || password.length() < 4 || password.length() > 10){
            edtPasswor.setError("between 4 and 10 alphanumeric character");
            valid = false;
        }else{
            edtPasswor.setError(null);
        }

        return valid;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
