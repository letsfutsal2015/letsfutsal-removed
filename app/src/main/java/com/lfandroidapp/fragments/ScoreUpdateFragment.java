package com.lfandroidapp.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.lfandroidapp.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class ScoreUpdateFragment extends Fragment {

    public static String title(){
        return "Score Update";
    }

    public static ScoreUpdateFragment newInstance(){
        return new ScoreUpdateFragment();
    }


    public ScoreUpdateFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_score_update, container, false);
    }


}
