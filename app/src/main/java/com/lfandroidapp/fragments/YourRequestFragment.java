package com.lfandroidapp.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.lfandroidapp.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class YourRequestFragment extends Fragment {

    public static String title(){
        return "Your Request";
    }

    public static YourRequestFragment newInstance(){
        return new YourRequestFragment();
    }

    public YourRequestFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_your_request, container, false);
    }


}
