package com.lfandroidapp.DrawerItems;

import android.support.annotation.LayoutRes;
import android.support.v7.widget.RecyclerView;

import com.lfandroidapp.R;
import com.mikepenz.materialdrawer.holder.ColorHolder;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;

/**
 * Created by vaio on 20/09/2015.
 */
public class TeamDrawerItem extends PrimaryDrawerItem {
    private ColorHolder background;

    public TeamDrawerItem withBackgroundColor(int backgroundColor) {
        this.background = ColorHolder.fromColor(backgroundColor);
        return this;
    }

    public TeamDrawerItem withBackgroundRes(int backgroundRes) {
        this.background = ColorHolder.fromColorRes(backgroundRes);
        return this;
    }

    @Override
    public void bindView(RecyclerView.ViewHolder holder) {
        super.bindView(holder);

        if (background != null) {
            background.applyToBackground(holder.itemView);
        }
    }

    @Override
    @LayoutRes
    public int getLayoutRes() {
        return R.layout.team_drawer_item;
    }
}
