package com.lfandroidapp.DrawerItems;

import android.support.annotation.LayoutRes;
import android.support.v7.widget.RecyclerView;

import com.lfandroidapp.R;
import com.mikepenz.materialdrawer.holder.ColorHolder;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;

/**
 * Created by vaio on 14/10/2015.
 */
public class CustomDrawerItem extends PrimaryDrawerItem {
    private ColorHolder background;

    public CustomDrawerItem withBackgroundColor(int backgroundColor) {
        this.background = ColorHolder.fromColor(backgroundColor);
        return this;
    }

    public CustomDrawerItem withBackgroundRes(int backgroundRes) {
        this.background = ColorHolder.fromColorRes(backgroundRes);
        return this;
    }

    @Override
    public void bindView(RecyclerView.ViewHolder holder) {
        super.bindView(holder);

        if (background != null) {
            background.applyToBackground(holder.itemView);
        }
    }

    @Override
    @LayoutRes
    public int getLayoutRes() {
        return R.layout.right_drawer_item;
    }
}
