package com.lfandroidapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.lfandroidapp.adapters.FindTeamAdapter;
import com.lfandroidapp.adapters.SubFindTeamAdapter;
import com.lfandroidapp.entities.Player;
import com.mikepenz.google_material_typeface_library.GoogleMaterial;

import java.util.ArrayList;

public class AddTeamMateActivity extends AppCompatActivity {

    private ArrayList<Player> primaryTeams;
    private ArrayList<Player> subTeams;

    private ListView listView;
    private ListView subListView;
    private FindTeamAdapter adapter;
    private SubFindTeamAdapter subAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_team_mate);

        //Handle toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_add_team_mate);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        //add prim player
        buildPrimaryPlayers();
        buildPrimaryAdapter(primaryTeams);

        //add sub player
        buildSubPlayers();
        buildSubAdapter(subTeams);

    }


    public void showToast(String message) {
        Toast mToast = Toast.makeText(this, message, Toast.LENGTH_SHORT);
        mToast.show();
    }



    private void buildPrimaryPlayers(){
        primaryTeams = new ArrayList<Player>();
        primaryTeams.add(new Player("Sandro", false, R.drawable.sandro));
        primaryTeams.add(new Player("Puji", false, R.drawable.puji));
        primaryTeams.add(new Player("Dimas", false, R.drawable.dimas));
        int sisa = 5 - primaryTeams.size();
        if(primaryTeams.size() < 5){
            for(int i=1; i < 5 - sisa; i++) {
                primaryTeams.add(new Player("Invite your friend to join team", false, R.drawable.ic_camera_alt_black_24dp, true));
            }
        }
    }

    private void buildPrimaryAdapter(ArrayList<Player> playerList){
        adapter = new FindTeamAdapter(this, playerList);
        listView = (ListView)findViewById(R.id.list_prim_team);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                showToast("test" + position);
                setAdminPlayer(position);
            }
        });
    }

    private void buildSubPlayers(){
        subTeams = new ArrayList<Player>();

        subTeams.add(new Player("Invite your friend to join team", false, R.drawable.ic_camera_alt_black_24dp, true));
    }

    private void buildSubAdapter(ArrayList<Player> playerList){
        subAdapter = new SubFindTeamAdapter(this, playerList);
        subListView = (ListView)findViewById(R.id.list_sub_team);
        subListView.setAdapter(subAdapter);

        subListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                showToast("test" + position);
            }
        });
    }

    private void setAdminPlayer(int position){
        Player p = primaryTeams.get(position);
        if(p.getIsCaptain()){
            p.setIsCaptain(false);
        }else{
            p.setIsCaptain(true);
        }

        primaryTeams.set(position, p);
        adapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add_team_mate, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
