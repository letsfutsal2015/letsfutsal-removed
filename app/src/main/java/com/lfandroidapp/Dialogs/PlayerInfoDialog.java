package com.lfandroidapp.Dialogs;

import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.lfandroidapp.MyTeamActivity;
import com.lfandroidapp.R;
import com.lfandroidapp.entities.Player;

public class PlayerInfoDialog extends DialogFragment {



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_player_info_dialog, container,false);

        ImageView imageView = (ImageView) rootView.findViewById(R.id.playerImage);
        TextView txtPlayerName = (TextView) rootView.findViewById(R.id.txtPlayerName);
        TextView txtTeamName = (TextView) rootView.findViewById(R.id.txtTeamName);
        TextView txtFacebook = (TextView) rootView.findViewById(R.id.txtFacebook);
        TextView txtWhatsapp = (TextView)rootView.findViewById(R.id.txtWhatsapp);

        //check bundle
        Bundle bundle = getArguments();
        Player player = (Player) bundle.getSerializable("PLAYER");
        final String pos = bundle.getString("POSITION");

        txtPlayerName.setText(player.getPlayerName());

        txtTeamName.setText(player.getTeamName());

        txtFacebook.setText(player.getFacebook());

        txtWhatsapp.setText(player.getWhatsapp());

        imageView.setImageResource(player.getImage());

        Button btnClose = (Button)rootView.findViewById(R.id.btnClose);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        Button btnChange = (Button)rootView.findViewById(R.id.btnChange);
        btnChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyTeamActivity callingActivity =(MyTeamActivity) getActivity();
//                callingActivity.setResult(Activity.RESULT_OK);
                callingActivity.invisibleChangeButtonInSubPlayerAdapter(false,pos);
                dismiss();
                //update listview adapter
            }
        });
        getDialog().setTitle("Player Info");
        // Do something else
        return rootView;
    }

}
