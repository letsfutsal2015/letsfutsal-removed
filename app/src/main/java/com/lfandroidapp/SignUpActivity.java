package com.lfandroidapp;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class SignUpActivity extends AppCompatActivity {

    private static final String TAG = "SignUpActivity";

    @Bind(R.id.input_username)
    EditText edtUsername;

    @Bind(R.id.input_firstname)
    EditText edtFirstname;

    @Bind(R.id.input_lastname)
    EditText edtLastname;

    @Bind(R.id.input_email)
    EditText edtEmail;

    @Bind(R.id.input_phone_number)
    EditText edtPhonenumber;

    @Bind(R.id.input_password)
    EditText edtPassword;

    @Bind(R.id.input_confirm_password)
    EditText edtConfirmPassword;

    @Bind(R.id.btn_signup)
    Button btnSignup;

    @Bind(R.id.link_login)
    TextView linkLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        ButterKnife.bind(this);

        btnSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signup();
            }
        });

        linkLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void signup(){
        Log.d(TAG, "signup");

        if(!validate()){
            onSignupFailed();
            return;
        }

        btnSignup.setEnabled(false);

        final ProgressDialog progressDialog = new ProgressDialog(SignUpActivity.this, R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Creating account ...");
        progressDialog.show();

        new android.os.Handler().postDelayed(
                new Runnable() {
                    @Override
                    public void run() {
                        saveToParse();
                        progressDialog.dismiss();
                    }
                }, 3000
        );
    }

    private void saveToParse(){
        String username = edtUsername.getText().toString();
        String firstname  = edtFirstname.getText().toString();
        String lastname = edtLastname.getText().toString();
        String phonenumber = edtPhonenumber.getText().toString();
        String password = edtPassword.getText().toString();
        String email = edtEmail.getText().toString();

        ParseUser user = new ParseUser();
        user.setUsername(username);
        user.setEmail(email);
        user.setPassword(password);
        user.put("firstname", firstname);
        user.put("lastname", lastname);
        user.put("phonenumber", phonenumber);
        user.signUpInBackground(new SignUpCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                   onSignupSuccess();
                } else {
                   onSignupFailed();
                }
            }
        });
    }

    private void onSignupSuccess(){
        Toast.makeText(getApplicationContext(), "Successfully Signed up.",Toast.LENGTH_LONG).show();
        btnSignup.setEnabled(true);
        setResult(RESULT_OK, null);

        finish();
    }

    private void onSignupFailed(){
        Toast.makeText(getBaseContext(), "Login failed", Toast.LENGTH_LONG).show();
        btnSignup.setEnabled(true);
    }

    private boolean validate(){
        boolean valid = true;

        String username = edtUsername.getText().toString();
        String firstname = edtFirstname.getText().toString();
        String lastname = edtLastname.getText().toString();
        String phonenumber = edtPhonenumber.getText().toString();
        String password = edtPassword.getText().toString();
        String confirmPass = edtConfirmPassword.getText().toString();
        String email = edtEmail.getText().toString();

        if(username.isEmpty() || username.length() < 5){
            edtUsername.setError("at least 5 character");
            valid = false;
        }else{
            edtUsername.setError(null);
        }

        if(firstname.isEmpty() || firstname.length() < 5){
            edtFirstname.setError("at least 5 character");
            valid = false;
        }else{
            edtFirstname.setError(null);
        }


        if(lastname.isEmpty() || lastname.length() < 5){
            edtLastname.setError("at least 5 character");
            valid = false;
        }else{
            edtLastname.setError(null);
        }

        if(phonenumber.isEmpty() || phonenumber.length() < 10){
            edtPhonenumber.setError("at least 10 character");
            valid = false;
        }else{
            edtPhonenumber.setError(null);
        }

        if(email.isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            edtEmail.setError("enter a valid email address");
            valid = false;
        }else{
            edtEmail.setError(null);
        }

        if(password.isEmpty() || password.length() < 4 || password.length() > 10){
            edtPassword.setError("between 4 and 10 alphanumeric characters");
            valid = false;
        }else{
            edtPassword.setError(null);
        }

        if(!password.equals(confirmPass)){
            edtConfirmPassword.setError("confirm password not same with password");
            valid = false;
        }else{
            edtConfirmPassword.setError(null);
        }

        //check existing user
        final boolean[] duplicate = {false};
        ParseQuery<ParseUser> query = ParseUser.getQuery();
        query.whereEqualTo("email", email);
        query.findInBackground(new FindCallback<ParseUser>() {
            @Override
            public void done(List<ParseUser> objects, ParseException e) {
                if(e == null){
                    if(objects.size() > 0){
                        duplicate[0] = true;
                    }
                }else{
                    onSignupFailed();
                }
            }
        });
        if(duplicate[0] == true){
            valid = false;
            Toast.makeText(getApplicationContext(), "Duplicate User.",Toast.LENGTH_LONG).show();
        }
        return valid;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_sign_up, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
