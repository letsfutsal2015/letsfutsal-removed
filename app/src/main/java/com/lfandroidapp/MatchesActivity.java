package com.lfandroidapp;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;

import com.lfandroidapp.adapters.PinUtils;
import com.lfandroidapp.fragments.OpponentRequestFragment;
import com.lfandroidapp.fragments.ScoreConfirmFragment;
import com.lfandroidapp.fragments.ScoreUpdateFragment;
import com.lfandroidapp.fragments.YourRequestFragment;

import java.util.Locale;

import it.neokree.materialtabs.MaterialTab;
import it.neokree.materialtabs.MaterialTabHost;
import it.neokree.materialtabs.MaterialTabListener;

public class MatchesActivity extends AppCompatActivity  implements MaterialTabListener {

    SectionsPagerAdapter adapter;
    ViewPager pager;
    MaterialTabHost tabHost;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_matches);

        //Handle toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_matches);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        this.setSupportActionBar(toolbar);


        tabHost = (MaterialTabHost) this.findViewById(R.id.tabHost);
        pager = (ViewPager) this.findViewById(R.id.pager );


        // init view pager
        adapter = new SectionsPagerAdapter(getSupportFragmentManager());
        pager.setAdapter(adapter);
        pager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                // when user do a swipe the selected tab change
                tabHost.setSelectedNavigationItem(position);

            }
        });
        // insert all tabs from pagerAdapter data
        for (int i = 0; i < adapter.getCount(); i++) {
            tabHost.addTab(
                    tabHost.newTab()
                            .setText(adapter.getPageTitle(i))
                            .setTabListener(this)
            );

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_matches, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.schedule) {
            callScheduleMatches();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void callScheduleMatches(){
        Intent intent = new Intent(this, ScheduleMatchesActivity.class);
        if(intent != null){
            startActivity(intent);
        }
    }

    @Override
    public void onTabSelected(MaterialTab materialTab) {

    }

    @Override
    public void onTabReselected(MaterialTab materialTab) {
        pager.setCurrentItem(materialTab.getPosition());
    }

    @Override
    public void onTabUnselected(MaterialTab materialTab) {

    }


    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return OpponentRequestFragment.newInstance();
                case 1:
                    return YourRequestFragment.newInstance();
                case 2:
                    return ScoreUpdateFragment.newInstance();
                case 3:
                    return ScoreConfirmFragment.newInstance();
            }
            return null;
        }

        @Override
        public int getCount() {
            return 4;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            Locale l = Locale.getDefault();
            switch (position) {
                case 0:
                    return OpponentRequestFragment.title();
                case 1:
                    return YourRequestFragment.title();
                case 2:
                    return ScoreUpdateFragment.title();
                case 3:
                    return ScoreConfirmFragment.title();
            }
            return null;
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        PinUtils.hideKeyboard(this);
    }
}
