package com.lfandroidapp.entities;

import java.io.Serializable;

/**
 * Created by vaio on 20/09/2015.
 */
public class Player implements Serializable {
    private String teamName;
    private String whatsapp;
    private String playerName;
    private String position;
    private Boolean isCaptain;
    private Boolean isnew;
    private String facebook;
    private Integer image;

    public Player(){

    }

    public Player(String playerName, Boolean captain, Integer image){
        this.playerName = playerName;
        this.isCaptain = captain;
        this.image = image;
        this.isnew = false;
    }

    public Player(String playerName, Boolean captain, Integer image, Boolean isnew){
        this.playerName = playerName;
        this.isCaptain = captain;
        this.image = image;
        this.isnew = isnew;
    }

    public Player(String playerName, String teamName, String whatsapp, String facebook, Integer image, String position){
        this.playerName = playerName;
        this.teamName = teamName;
        this.whatsapp = whatsapp;
        this.facebook = facebook;
        this.image = image;
        this.position = position;
        this.isnew = false;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public String getWhatsapp() {
        return whatsapp;
    }

    public void setWhatsapp(String whatsapp) {
        this.whatsapp = whatsapp;
    }


    public Integer getImage() {
        return image;
    }

    public void setImage(Integer image) {
        this.image = image;
    }


    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public Boolean getIsCaptain() {
        return isCaptain;
    }

    public void setIsCaptain(Boolean isCaptain) {
        this.isCaptain = isCaptain;
    }

    public Boolean getIsnew() {
        return isnew;
    }

    public void setIsnew(Boolean isnew) {
        this.isnew = isnew;
    }
}
