package com.lfandroidapp.entities;

import java.io.Serializable;

/**
 * Created by vaio on 22/09/2015.
 */
public class Team implements Serializable{
    private Integer image;
    private String teamName;

    public Team(Integer image, String teamName){
        this.image = image;
        this.teamName = teamName;
    }

    public Integer getImage() {
        return image;
    }

    public void setImage(Integer image) {
        this.image = image;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }
}
