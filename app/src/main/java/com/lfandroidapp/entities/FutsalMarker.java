package com.lfandroidapp.entities;
import java.io.Serializable;

/**
 * Created by vaio on 01/10/2015.
 */
public class FutsalMarker implements Serializable{
    private String id;
    private String icon;
    private Double latitute;
    private Double longitude;

    public FutsalMarker(String id, String icon, Double latitute, Double longitude){
        this.id = id;
        this.latitute = latitute;
        this.longitude = longitude;
        this.icon = icon;
    }

    public FutsalMarker(){
        this.id = "";
        this.latitute = 0.0;
        this.longitude = 0.0;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Double getLatitute() {
        return latitute;
    }

    public void setLatitute(Double latitute) {
        this.latitute = latitute;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
}
