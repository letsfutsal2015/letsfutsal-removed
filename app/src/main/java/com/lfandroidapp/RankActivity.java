package com.lfandroidapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.ListView;

import com.lfandroidapp.adapters.RankAdapter;
import com.lfandroidapp.entities.Rank;

import java.util.ArrayList;

public class RankActivity extends AppCompatActivity {

    private ListView listGeneral;
    private ListView listEvent;
    private RankAdapter rankAdapter;

    private ArrayList<Rank> generalList;
    private ArrayList<Rank> eventList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rank);

        //Handle toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_rank);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        //add general
        generalList = new ArrayList<Rank>();
        generalList.add(new Rank("Indonesia", 200));
        generalList.add(new Rank("Jakarta", 10));
        buildGeneral(generalList);

        eventList = new ArrayList<Rank>();
        eventList.add(new Rank("Planet Futsal Kuningan", 2));
        eventList.add(new Rank("De Futsal",1));
        buildEvent(eventList);
        //add event
    }

    private void buildGeneral(ArrayList<Rank> list){
        rankAdapter = new RankAdapter(this, list);

        listGeneral = (ListView)findViewById(R.id.list_general_category);
        listGeneral.setAdapter(rankAdapter);
    }

    private void buildEvent(ArrayList<Rank> list){
        rankAdapter = new RankAdapter(this, list);

        listEvent = (ListView)findViewById(R.id.list_event);
        listEvent.setAdapter(rankAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_rank, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
