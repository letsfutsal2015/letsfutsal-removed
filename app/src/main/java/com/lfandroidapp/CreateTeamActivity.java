package com.lfandroidapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.lfandroidapp.adapters.NothingSelectedSpinnerAdapter;

public class CreateTeamActivity extends AppCompatActivity {
    private Spinner spinnerLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_team);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_create_team);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        String[] items = {"Jakarta", "Bandung"};
        String[] categories = {"BOYS","GIRLS"};
        buildSpinnerLocation(items);
        buildSpinnerCategory(categories);

    }


    private void buildSpinnerLocation(String[] strings){
        spinnerLocation = (Spinner) findViewById(R.id.spinnerLocation);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.spinner_location, strings);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerLocation.setPrompt("City Location");
        spinnerLocation.setAdapter(new NothingSelectedSpinnerAdapter(adapter, R.layout.spinner_location_nothing_selected, this));
    }

    private void buildSpinnerCategory(String[] strings){
        spinnerLocation = (Spinner) findViewById(R.id.spinnerCategory);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.spinner_location, strings);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerLocation.setPrompt("Category");
        spinnerLocation.setAdapter(new NothingSelectedSpinnerAdapter(adapter, R.layout.spinner_category_nothing_selected, this));
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_create_team, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_next) {
            callAddTeamMate();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void callAddTeamMate(){
        Intent intent = new Intent(this, AddTeamMateActivity.class);
        startActivity(intent);
    }
}
