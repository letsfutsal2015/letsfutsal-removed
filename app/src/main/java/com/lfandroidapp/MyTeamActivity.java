package com.lfandroidapp;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.lfandroidapp.Dialogs.PlayerInfoDialog;
import com.lfandroidapp.DrawerItems.CustomDrawerItem;
import com.lfandroidapp.DrawerItems.TeamDrawerItem;
import com.lfandroidapp.adapters.SubPlayerAdapter;
import com.lfandroidapp.entities.Player;
import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.google_material_typeface_library.GoogleMaterial;

import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.holder.BadgeStyle;
import com.mikepenz.materialdrawer.holder.StringHolder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;
import com.parse.GetCallback;
import com.parse.GetDataCallback;
import com.parse.ParseAnonymousUtils;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class MyTeamActivity extends AppCompatActivity {
    //position
    final static String KEEPER = "KEEPER";
    final static String LEFT_WING = "LEFT_WING";
    final static String RIGHT_WING = "RIGHT_WING";
    final static String LEFT_STRIKER = "LEFT_STRIKER";
    final static String RIGHT_STRIKER = "RIGHT_STRIKER";

    //identifier right menu
    final static Integer CREATE_MATCH = 999;
    final static Integer MATCHES = 888;
    final static Integer RANK = 777;
    final static Integer NEWSEVENT = 666;
    final static Integer SETTINGS = 555;
    final static Integer FUTSAL_STATION = 444;
    final static Integer MESSAGES = 99;
    final static Integer MY_ACCOUNT = 100;

    private AccountHeader headerResult = null;
    private Drawer rightDrawerMenu = null;
    private Drawer leftDrawerMenu = null;

    private IProfile profile1, profile2;
    private CircleImageView btnKeeper, btnLeftWing, btnRightWing, btnLeftStriker, btnRightStriker;

    FragmentManager fm = null;

    Player pKeeper, pLeftWing, pRightWing, pLeftStriker, pRightStriker;
    TextView txtKeeper, txtLeftWing, txtLeftStriker, txtRightWing, txtRightStriker;

    ArrayList<Player> primTeams;
    ArrayList<Player> subTeams;

    SubPlayerAdapter subPlayerAdapter;
    ListView listView;

    PlayerInfoDialog playerInfo;

    private String flagReplacedPlayer;

    private List<IDrawerItem> teamList;

    ParseUser currentUser;

    Drawable d = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_team);

        //declaration
        fm = this.getFragmentManager();
        initilaizeLayoutComponents();
        initializePlayer();

        //Handle toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_perm_identity_black_24dp);
        setSupportActionBar(toolbar);

        if (ParseAnonymousUtils.isLinked(ParseUser.getCurrentUser())) {
            // If user is anonymous, send the user to LoginSignupActivity.class
            Intent intent = new Intent(MyTeamActivity.this, LoginActivity.class);
            startActivityForResult(intent, RESULT_OK);
        } else {
            currentUser = ParseUser.getCurrentUser();
            if (currentUser == null) {
                Intent intent = new Intent(MyTeamActivity.this, LoginActivity.class);
                startActivityForResult(intent, RESULT_OK);
            }


            //        buildHeader(false, savedInstanceState);
            buildLeftDrawer(false, savedInstanceState, toolbar);
            builderRightDrawer(false, savedInstanceState, null);
            buildSubPlayerAdapter();

            //set the back arrow in the toolbar
            getSupportActionBar().setDisplayShowHomeEnabled(true);

            //put event listener here
            eventListeners();

            final Bundle b = savedInstanceState;

            new android.os.Handler().postDelayed(
                    new Runnable() {
                        @Override
                        public void run() {
                            getImageFromParse(b);
                        }
                    }, 3000
            );
        }
    }

    private void getImageFromParse(Bundle savedInstanceStateTemp) {
        final Bundle savedInstanceState = savedInstanceStateTemp;
        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>("ImageUpload");
        query.whereEqualTo("users", currentUser);
        query.getFirstInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject object, ParseException e) {
                if (object != null) {
                    ParseFile image = (ParseFile) object.get("imageFile");

                    image.getDataInBackground(new GetDataCallback() {
                        @Override
                        public void done(byte[] data, ParseException e) {
                            if (e == null) {
                                Bitmap map = BitmapFactory.decodeByteArray(data, 0, data.length);
                                d = new BitmapDrawable(getResources(), map);

                                //add team profile as header`
                                if (currentUser != null) {
                                    profile1 = new ProfileDrawerItem().withName(currentUser.getUsername()).withEmail(currentUser.getEmail()).withIcon(d);
                                    getSupportActionBar().setTitle(currentUser.getUsername());
                                } else {
                                    profile1 = new ProfileDrawerItem().withName("quest").withEmail("").withIcon(getResources().getDrawable(R.drawable.ic_face_white_24dp));
                                }

                                buildHeader(true, savedInstanceState);

                            }
                        }
                    });
                }
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_my_team, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_clear) {
            hideClearBtn = true;
            refreshListView(hideClearBtn);
            return true;
        } else if (id == R.id.action_right_menu) {
            rightDrawerMenu.openDrawer();
        }

        return super.onOptionsItemSelected(item);
    }

    boolean hideClearBtn = true;

    @Override
    protected boolean onPrepareOptionsPanel(View view, Menu menu) {
        if (hideClearBtn) {
            menu.getItem(0).setVisible(false);
            menu.getItem(1).setVisible(true);
        } else {
            menu.getItem(0).setVisible(true);
            menu.getItem(1).setVisible(false);
        }

        return true;
    }


    private void buildHeader(boolean compact, Bundle savedInstanceState) {
        // Create the AccountHeader
        headerResult = new AccountHeaderBuilder()
                .withActivity(this)
                .withHeaderBackground(R.drawable.header)
                .withCompactStyle(compact)
                .addProfiles(
                        profile1
                )
                .withSavedInstance(savedInstanceState)
                .build();
    }

    private void buildLeftDrawer(boolean compact, Bundle savedInstanceState, Toolbar toolbar) {
        teamList = new ArrayList<IDrawerItem>();
//        teamList.add(new TeamDrawerItem().withName("ARSENAL").withIcon(R.drawable.arsenal));
//        teamList.add(new TeamDrawerItem().withName("MANCHESTER UNITED").withIcon(R.drawable.manchester_united));
//        teamList.add(new TeamDrawerItem().withName("LIVERPOOL").withIcon(R.drawable.liverpool));

        leftDrawerMenu = new DrawerBuilder()
                .withActivity(this)
                .withAccountHeader(headerResult)
                .withToolbar(toolbar)
                .withSavedInstance(savedInstanceState)
                .withSliderBackgroundColorRes(R.color.colorAccent)
                .addStickyDrawerItems(
                        new SecondaryDrawerItem().withName("MY ACCOUNT").withIcon(GoogleMaterial.Icon.gmd_perm_identity).withIdentifier(MY_ACCOUNT)
                )
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int i, IDrawerItem drawerItem) {
                        if (drawerItem != null) {
                            if (drawerItem.getIdentifier() == MESSAGES) {
                                callMessages();
                            } else if (drawerItem.getIdentifier() == MY_ACCOUNT) {
                                callMyAccountActivity();
                            } else {
                                if (i > 2) {
                                    TeamDrawerItem team = (TeamDrawerItem) teamList.get(i - 3);
                                    if (team != null) {
                                        showToast(team.getName().getText());
                                        changeActionBarTitle(team.getName().getText());
                                    }
                                }
                            }

                        }
                        return false;
                    }
                })
                .build();

        leftDrawerMenu.addItems(new SecondaryDrawerItem().withName("MESSAGES").withIcon(GoogleMaterial.Icon.gmd_email).withBadgeStyle(new BadgeStyle().withTextColor(Color.RED).withColorRes(R.color.md_red_100)).withIdentifier(MESSAGES).withSelectable(true).withIdentifier(MESSAGES));
        leftDrawerMenu.addItems(new DividerDrawerItem().withIdentifier(-1));
        for (IDrawerItem di : teamList) {
            leftDrawerMenu.addItem(di);
        }


        leftDrawerMenu.updateBadge(MESSAGES, new StringHolder(4 + " "));
    }

    private void changeActionBarTitle(String title) {
        getSupportActionBar().setTitle(title);
    }

    private void callMyAccountActivity() {
        Intent intent = new Intent(this, MyAccountActivity.class);
        if (intent != null) {
            this.startActivity(intent);
        }
    }

    private void callMessages() {
        Intent intent = new Intent(this, MessageActivity.class);
        if (intent != null) {
            this.startActivity(intent);
        }
    }

    private void callCreateMatch() {
        Intent intent = new Intent(this, CreateMatchActivity.class);
        if (intent != null) {
            this.startActivity(intent);
        }
    }

    private void callRank() {
        Intent intent = new Intent(this, RankActivity.class);
        if (intent != null) {
            this.startActivity(intent);
        }
    }

    private void callFutsalStation() {
        Intent intent = new Intent(this, FutsalStationMapActivity.class);
        if (intent != null) {
            this.startActivity(intent);
        }
    }

    private void callMatches() {
        Intent intent = new Intent(this, MatchesActivity.class);
        if (intent != null) {
            this.startActivity(intent);
        }
    }

    private void callNewsEvent() {
        Intent intent = new Intent(this, NewsEventActivity.class);
        if (intent != null) {
            this.startActivity(intent);
        }
    }

    private void callSetting() {
        Intent intent = new Intent(this, SettingActivity.class);
        if (intent != null) {
            this.startActivity(intent);
        }
    }


    private void builderRightDrawer(boolean compact, Bundle savedInstanceState, Toolbar toolbar) {
        rightDrawerMenu = new DrawerBuilder()
                .withActivity(this)
                .withDisplayBelowStatusBar(true)
                .withSavedInstance(savedInstanceState)
                .withSliderBackgroundColorRes(R.color.colorAccent)
                .addDrawerItems(
                        new CustomDrawerItem().withName("Create Match").withSelectable(false).withIdentifier(CREATE_MATCH),
                        new CustomDrawerItem().withName("Matches").withIcon(R.drawable.matches).withSelectable(false).withIdentifier(MATCHES),
                        new CustomDrawerItem().withName("Rank").withIcon(R.drawable.rank).withSelectable(false).withIdentifier(RANK),
                        new CustomDrawerItem().withName("Futsal Station").withIcon(R.drawable.fstation).withSelectable(false).withIdentifier(FUTSAL_STATION),
                        new CustomDrawerItem().withName("New/Event").withIcon(R.drawable.news).withSelectable(false).withIdentifier(NEWSEVENT)
                ).addStickyDrawerItems(
                        new SecondaryDrawerItem().withName(R.string.drawer_item_settings).withIcon(FontAwesome.Icon.faw_cog).withIdentifier(SETTINGS)
                )
                .withDrawerGravity(Gravity.END)
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int i, IDrawerItem iDrawerItem) {
                        if (iDrawerItem.getIdentifier() == CREATE_MATCH) {
                            callCreateMatch();
                        } else if (iDrawerItem.getIdentifier() == MATCHES) {
                            callMatches();
                        } else if (iDrawerItem.getIdentifier() == RANK) {
                            callRank();
                        } else if (iDrawerItem.getIdentifier() == FUTSAL_STATION) {
                            callFutsalStation();
                        } else if (iDrawerItem.getIdentifier() == NEWSEVENT) {
                            callNewsEvent();
                        } else if (iDrawerItem.getIdentifier() == SETTINGS) {
                            callSetting();
                        } else {
                            showToast("not found");
                        }
                        showToast("position " + i);
                        return false;
                    }
                })
                .append(leftDrawerMenu);

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        //add the values which need to be saved from the drawer to the bundle
        if (leftDrawerMenu != null) {
            outState = leftDrawerMenu.saveInstanceState(outState);
        }
        //add the values which need to be saved from the accountHeader to the bundle
        if (headerResult != null) {
            outState = headerResult.saveInstanceState(outState);
        }
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onBackPressed() {
        //handle the back press :D close the drawer first and if the drawer is closed close the activity
        if (leftDrawerMenu != null && leftDrawerMenu.isDrawerOpen()) {
            leftDrawerMenu.closeDrawer();
        } else {
            super.onBackPressed();
        }
    }


    private void showPlayerDialog(Player player, String pos) {
        playerInfo = new PlayerInfoDialog();
        Bundle bundle = new Bundle();
        bundle.putSerializable("PLAYER", player);
        bundle.putString("POSITION", pos);

        //parsing to next fragment
        playerInfo.setArguments(bundle);
        playerInfo.show(fm, "Player Info");
    }

    private void initializePlayer() {
        pKeeper = new Player();
        pLeftWing = new Player();
        pRightWing = new Player();
        pLeftStriker = new Player();
        pRightStriker = new Player();

        //temp data
        primTeams = new ArrayList<Player>();
//        primTeams.add(new Player("Indra", "ARSENAL", "0862xxxxxx","Indra.fb", R.drawable.indra, LEFT_WING));
//        primTeams.add(new Player("Dinno", "ARSENAL", "0861xxxxxx", "Dinno.fb",R.drawable.dinno, LEFT_STRIKER));
//        primTeams.add(new Player("Sandro", "ARSENAL", "0861xxxxxx", "Sandro.fb", R.drawable.sandro, KEEPER));
//        primTeams.add(new Player("Dimas", "ARSENAL", "0861xxxxxx","Dimas.fb", R.drawable.dimas, RIGHT_STRIKER));
//        primTeams.add(new Player("Puji", "ARSENAL", "0861xxxxxx","Puji.fb", R.drawable.puji, RIGHT_WING));

        subTeams = new ArrayList<Player>();
//        subTeams.add(new Player("Android 1", "ARSENAL", "0861xxxxxx", "android1.fb", R.drawable.ic_launcher, KEEPER));
//        subTeams.add(new Player("Android 2", "ARSENAL", "0861xxxxxx", "android2.fb", R.drawable.ic_launcher, KEEPER));

        //set value to fields

        for (Player p : primTeams) {
            if (p != null) {
                if (p.getPosition().equals(KEEPER)) {
                    txtKeeper.setText(p.getPlayerName());
                    btnKeeper.setImageResource(p.getImage());
                    pKeeper = p;
                } else if (p.getPosition().equals(LEFT_WING)) {
                    txtLeftWing.setText(p.getPlayerName());
                    btnLeftWing.setImageResource(p.getImage());
                    pLeftWing = p;
                } else if (p.getPosition().equals(RIGHT_WING)) {
                    txtRightWing.setText(p.getPlayerName());
                    btnRightWing.setImageResource(p.getImage());
                    pRightWing = p;
                } else if (p.getPosition().equals(LEFT_STRIKER)) {
                    txtLeftStriker.setText(p.getPlayerName());
                    btnLeftStriker.setImageResource(p.getImage());
                    pLeftStriker = p;
                } else if (p.getPosition().equals(RIGHT_STRIKER)) {
                    txtRightStriker.setText(p.getPlayerName());
                    btnRightStriker.setImageResource(p.getImage());
                    pRightStriker = p;
                } else {
                    //do nothing
                }
            }
        }
    }

    private void initilaizeLayoutComponents() {
        btnKeeper = (CircleImageView) findViewById(R.id.btnKeeper);
        btnLeftWing = (CircleImageView) findViewById(R.id.btnLeftWing);
        btnRightWing = (CircleImageView) findViewById(R.id.btnRightWing);
        btnLeftStriker = (CircleImageView) findViewById(R.id.btnLeftStriker);
        btnRightStriker = (CircleImageView) findViewById(R.id.btnRightStriker);

        txtKeeper = (TextView) findViewById(R.id.txtKeeperName);
        txtLeftWing = (TextView) findViewById(R.id.txtLeftWingName);
        txtRightWing = (TextView) findViewById(R.id.txtRightWingName);
        txtLeftStriker = (TextView) findViewById(R.id.txtLeftStrikerName);
        txtRightStriker = (TextView) findViewById(R.id.txtRightStrikerName);
    }

    private void eventListeners() {
        btnKeeper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPlayerDialog(pKeeper, KEEPER);
            }
        });

        btnLeftWing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPlayerDialog(pLeftWing, LEFT_WING);
            }
        });

        btnRightWing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPlayerDialog(pRightWing, RIGHT_WING);
            }
        });

        btnLeftStriker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPlayerDialog(pLeftStriker, LEFT_STRIKER);
            }
        });

        btnRightStriker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPlayerDialog(pRightStriker, RIGHT_STRIKER);
            }
        });
    }

    private void buildSubPlayerAdapter() {
        subPlayerAdapter = new SubPlayerAdapter(this, subTeams);
        listView = (ListView) findViewById(R.id.list_my_team);
        listView.setAdapter(subPlayerAdapter);

    }


    public void showToast(String message) {
        Toast mToast = Toast.makeText(this, message, Toast.LENGTH_SHORT);
        mToast.show();
    }


    public void invisibleChangeButtonInSubPlayerAdapter(Boolean bol, String flagPosition) {
        hideClearBtn = bol;
        subPlayerAdapter.setInvisibleBtnChange(bol);
        subPlayerAdapter.notifyDataSetChanged();
        listView.setAdapter(subPlayerAdapter);

        flagReplacedPlayer = flagPosition;
        invalidateOptionsMenu();

    }

    public void refreshListView(Boolean bol) {
        hideClearBtn = bol;
        subPlayerAdapter.setInvisibleBtnChange(bol);
        subPlayerAdapter.notifyDataSetChanged();
        listView.setAdapter(subPlayerAdapter);
        invalidateOptionsMenu();

    }

    public String replacePlayer(Player subPlayer) {
        String result = "";

        switch (flagReplacedPlayer) {
            case KEEPER:
                replaceKeeper(subPlayer);
                result = KEEPER;
                break;
            case LEFT_WING:
                replaceLeftWing(subPlayer);
                result = LEFT_WING;
                break;
            case LEFT_STRIKER:
                replaceLeftStriker(subPlayer);
                result = LEFT_STRIKER;
                break;
            case RIGHT_WING:
                replaceRightWing(subPlayer);
                result = RIGHT_WING;
                break;
            case RIGHT_STRIKER:
                replaceRightStriker(subPlayer);
                result = RIGHT_STRIKER;
                break;
        }

        return result;

    }

    public void replaceKeeper(Player subPlayer) {
        btnKeeper.setImageResource(subPlayer.getImage());
        txtKeeper.setText(subPlayer.getPlayerName());

        subTeams.remove(subPlayer);
        subTeams.add(pKeeper);

        pKeeper = subPlayer;
    }

    public void replaceLeftWing(Player subPlayer) {
        btnLeftWing.setImageResource(subPlayer.getImage());
        txtLeftWing.setText(subPlayer.getPlayerName());

        subTeams.remove(subPlayer);
        subTeams.add(pLeftWing);

        pLeftWing = subPlayer;
    }

    public void replaceLeftStriker(Player subPlayer) {
        btnLeftStriker.setImageResource(subPlayer.getImage());
        txtLeftStriker.setText(subPlayer.getPlayerName());

        subTeams.remove(subPlayer);
        subTeams.add(pLeftStriker);

        pLeftStriker = subPlayer;
    }

    public void replaceRightWing(Player subPlayer) {
        btnRightWing.setImageResource(subPlayer.getImage());
        txtRightWing.setText(subPlayer.getPlayerName());

        subTeams.remove(subPlayer);
        subTeams.add(pRightWing);

        pRightWing = subPlayer;
    }

    public void replaceRightStriker(Player subPlayer) {
        btnRightStriker.setImageResource(subPlayer.getImage());
        txtRightStriker.setText(subPlayer.getPlayerName());

        subTeams.remove(subPlayer);
        subTeams.add(pRightStriker);

        pRightStriker = subPlayer;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            currentUser = ParseUser.getCurrentUser();

        }
    }


}
