package com.lfandroidapp;

import android.app.ProgressDialog;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;

import com.dpizarro.pinview.library.PinView;
import com.dpizarro.pinview.library.PinViewSettings;
import com.lfandroidapp.adapters.PinUtils;

public class TeamCodeActivity extends AppCompatActivity {

    private PinView pinView;
    private ProgressDialog dialog;
    private View snackView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team_code);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_team_code);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        pinView = (PinView) findViewById(R.id.pinView);
        snackView = findViewById(R.id.snackbarPosition);

        String[] titles = getResources().getStringArray(R.array.key_titles);
        String[] titlesAux = new String[5];
        System.arraycopy(titles, 0, titlesAux, 0, 5);

        /**
         * Option using an easy Builder
         */
        PinViewSettings pinViewSettings = new PinViewSettings.Builder()
                .withPinTitles(titlesAux)
                .withMaskPassword(true)
                .withDeleteOnClick(true)
                .withKeyboardMandatory(false)
                .withSplit(null)
                .withNumberPinBoxes(5)
                .withNativePinBox(false)
                .build();

        pinView.setSettings(pinViewSettings);

        pinView.setOnCompleteListener(new PinView.OnCompleteListener() {
            @Override
            public void onComplete(boolean completed, final String pinResults) {
                if (completed) {
                    /**
                     * Do what you want
                     */
                    showProgressDialogLogin();
                    showSnackbar(pinResults);
                }
            }
        });

    }

    private void showProgressDialogLogin() {
        dialog = ProgressDialog.show(this,
                getResources().getString(R.string.login),
                getResources().getString(R.string.loading), true);
    }

    private void showSnackbar(final String pinResults) {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                dialog.cancel();
                PinUtils.hideKeyboard(getApplicationContext());

                Snackbar.make(snackView, getResources().getString(R.string.login_ok) + pinResults,
                        Snackbar.LENGTH_INDEFINITE)
                        .setAction(R.string.clear_pin, clickListener)
                        .show();
            }
        }, 2000);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_team_code, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private final View.OnClickListener clickListener = new View.OnClickListener() {
        public void onClick(View v) {
            pinView.clear();
        }
    };
}
