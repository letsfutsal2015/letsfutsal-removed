package com.lfandroidapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ListView;

import com.lfandroidapp.adapters.TeamMatchesScheduleAdapter;
import com.lfandroidapp.decorators.EventDecorator;
import com.lfandroidapp.entities.Team;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;

import butterknife.Bind;
import butterknife.ButterKnife;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class ScheduleMatchesActivity extends AppCompatActivity implements OnDateSelectedListener {

    private TeamMatchesScheduleAdapter teamMatchesScheduleAdapter;
    private ListView listView;

    @Bind(R.id.calendarView)
    MaterialCalendarView widget;

    Calendar calendar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedule_matches);
        ButterKnife.bind(this);

        //Handle toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_schedule);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        ArrayList<Team> teamList = new ArrayList<Team>();
        ArrayList<ArrayList<Team>> teams = new ArrayList<ArrayList<Team>>();
        teamList.add(new Team(R.drawable.arsenal, "ARSENAL"));
        teamList.add(new Team(R.drawable.liverpool, "LIVERPOOL"));
        teams.add(teamList);

        buildTeamMatchesAdapter(teams);
        widget.setOnDateChangedListener(this);

        calendar = Calendar.getInstance();
        CalendarDay calendarDay =  CalendarDay.from(calendar);
        ArrayList<CalendarDay> calList = new ArrayList<CalendarDay>();
        calList.add(calendarDay);
        widget.addDecorator(new EventDecorator(R.color.colorAccent,calList));

    }

    private void buildTeamMatchesAdapter(ArrayList<ArrayList<Team>> teams){
        teamMatchesScheduleAdapter = new TeamMatchesScheduleAdapter(this, teams);
        listView = (ListView)findViewById(R.id.list_matches_schedule);
        listView.setAdapter(teamMatchesScheduleAdapter);
        listView.setVisibility(View.INVISIBLE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_schedule_matches, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDateSelected(MaterialCalendarView materialCalendarView, CalendarDay calendarDay, boolean b) {
        if(calendarDay.getCalendar().get(Calendar.YEAR) == calendar.get(Calendar.YEAR) &&
                calendarDay.getCalendar().get(Calendar.MONTH) == calendar.get(Calendar.MONTH) &&
                calendarDay.getCalendar().get(Calendar.DAY_OF_MONTH) == calendar.get(Calendar.DAY_OF_MONTH)) {
            listView.setVisibility(View.VISIBLE);
        }else{
            listView.setVisibility(View.INVISIBLE);
        }
    }
}
