package com.lfandroidapp;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.lfandroidapp.adapters.MyTeamAdapter;
import com.lfandroidapp.entities.Team;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseRelation;
import com.parse.ParseUser;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class MyAccountActivity extends AppCompatActivity {

    int REQUEST_CAMERA = 0, SELECT_FILE = 1;

    private MyTeamAdapter teamAdapter;
    private ListView listView;

    private ArrayList<Team> teamList;

    private TextView btnCreate;
    private TextView btnJoinTeam;

    private CircleImageView imageView;

    private ParseUser currentUser;

    MaterialDialog progressDialog = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_account);

        //Handle toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_my_acc);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        //display progress dialog;
        progressDialog = new MaterialDialog.Builder(this)
                .content("Authenticating..")
                .progress(true, 0)
                .progressIndeterminateStyle(true)
                .show();

        imageView = (CircleImageView) findViewById(R.id.icPlayer);

        currentUser = ParseUser.getCurrentUser();

        //declare
        teamList = new ArrayList<Team>();
        teamList.add(new Team(R.drawable.arsenal, "ARSENAL"));
        teamList.add(new Team(R.drawable.liverpool, "LIVERPOOL"));
        teamList.add(new Team(R.drawable.manchester_united, "MANCHESTER UNITED"));
        for(int i=1; i < 2; i++){
            teamList.add(new Team(R.drawable.ic_launcher, "UNKNOWN"+i));
        }

        buildTeamAdapter(teamList);
        buildBtnListener();

        new android.os.Handler().postDelayed(
                new Runnable() {
                    @Override
                    public void run() {
                        getImageFromParse();
                    }
                }, 3000
        );



    }

    private void buildBtnListener(){
        imageView = (CircleImageView) findViewById(R.id.icPlayer);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.setType("image/*");
                startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
            }
        });

        btnCreate = (TextView) findViewById(R.id.btnCreate);
        btnCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callCreateTeamActivity(v);
            }
        });

        btnJoinTeam = (TextView) findViewById(R.id.btnJoin);
        btnJoinTeam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callTeamCode(v);
            }
        });
    }

    private void callCreateTeamActivity(View v){
        Intent i = new Intent(this, CreateTeamActivity.class);
        startActivity(i);
    }

    private void callTeamCode(View v){
        Intent i = new Intent(this, TeamCodeActivity.class);
        startActivity(i);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_my_account, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_done) {
            saveImage();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void buildTeamAdapter(ArrayList<Team> teamList){
        teamAdapter = new MyTeamAdapter(this, teamList);
        listView = (ListView)findViewById(R.id.list_my_team);
        listView.setAdapter(teamAdapter);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
        }
    }

    private byte[] imageByte;
    private String imagename;

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {
        Uri selectedImageUri = data.getData();
        String[] projection = { MediaStore.MediaColumns.DATA };
        Cursor cursor = managedQuery(selectedImageUri, projection, null, null,
                null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        cursor.moveToFirst();

        String selectedImagePath = cursor.getString(column_index);

        Bitmap bm;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(selectedImagePath, options);
        final int REQUIRED_SIZE = 200;
        int scale = 1;
        while (options.outWidth / scale / 2 >= REQUIRED_SIZE
                && options.outHeight / scale / 2 >= REQUIRED_SIZE)
            scale *= 2;
        options.inSampleSize = scale;
        options.inJustDecodeBounds = false;
        bm = BitmapFactory.decodeFile(selectedImagePath, options);

        // Convert it to byte
        ByteArrayOutputStream stream = new ByteArrayOutputStream();

        bm.compress(Bitmap.CompressFormat.PNG, 100, stream);

        File file = new File(selectedImagePath);

        imageByte = stream.toByteArray();

        imagename =file.getName();

        imageView.setImageBitmap(bm);
        hideDoneBtn = false;
        invalidateOptionsMenu();
    }

    private void saveImage(){

        final ParseFile file = new ParseFile(imagename, imageByte);
        file.saveInBackground();

        ParseQuery<ParseObject> query = ParseQuery.getQuery("ImageUpload");
        query.whereEqualTo("users", currentUser);

        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if (objects.size() > 0) {

                    ParseObject imgupload = new ParseObject("ImageUpload");
                    // Create a column named "ImageName" and set the string
                    imgupload.put("imageName", imagename);

                    // Create a column named "ImageFile" and insert the image
                    imgupload.put("imageFile", file);

                    imgupload.saveInBackground();
                } else {
                    ParseObject imgupload = new ParseObject("ImageUpload");
                    // Create a column named "ImageName" and set the string
                    imgupload.put("imageName", imagename);

                    // Create a column named "ImageFile" and insert the image
                    imgupload.put("imageFile", file);

                    //set relation with user
                    ParseRelation<ParseUser> relation = imgupload.getRelation("users");
                    relation.add(currentUser);

                    imgupload.saveInBackground();
                }
            }
        });

        // Show a simple toast message
        Toast.makeText(this, "Image Uploaded Successfully",Toast.LENGTH_SHORT).show();

        hideDoneBtn = true;
        invalidateOptionsMenu();

    }

    private boolean hideDoneBtn = true;
    @Override
    protected boolean onPrepareOptionsPanel(View view, Menu menu) {
        if(hideDoneBtn){
            menu.getItem(0).setVisible(false);
        }else {
            menu.getItem(0).setVisible(true);
        }

        return true;
    }

    private void getImageFromParse(){
        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>("ImageUpload");
        query.whereEqualTo("users", currentUser);
        query.getFirstInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject object, ParseException e) {
                if(object != null) {
                    ParseFile image = (ParseFile) object.get("imageFile");

                    image.getDataInBackground(new GetDataCallback() {
                        @Override
                        public void done(byte[] data, ParseException e) {
                            if (e == null) {
                                Bitmap map = BitmapFactory.decodeByteArray(data, 0, data.length);
                                imageView.setImageBitmap(map);
                                progressDialog.dismiss();
                            }
                        }
                    });
                }else{
                    progressDialog.dismiss();
                }
            }
        });

    }
}
