package com.lfandroidapp.adapters;

import android.app.Activity;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.lfandroidapp.R;
import com.lfandroidapp.entities.Rank;

import java.util.ArrayList;

/**
 * Created by vaio on 30/09/2015.
 */
public class RankAdapter extends ArrayAdapter<Rank>{
    private final Activity context;
    private final ArrayList<Rank> ranks;

    private TextView txtName;
    private TextView txtNumber;

    public RankAdapter(Activity context, ArrayList<Rank> ranks){
        super(context, R.layout.list_rank, ranks);
        this.context = context;
        this.ranks = ranks;
    }

    public View getView(int position, View view, ViewGroup parent){
        View rowview = view;
        final Rank rank = ranks.get(position);
        LayoutInflater inflater = context.getLayoutInflater();
        rowview = inflater.inflate(R.layout.list_rank, null, true);

        txtName = (TextView)rowview.findViewById(R.id.txtNameRank);
        txtNumber = (TextView)rowview.findViewById(R.id.txtNumber);

        txtName.setText(rank.getName());
        txtNumber.setText(rank.getNumber().toString());

        return rowview;
    }
}
