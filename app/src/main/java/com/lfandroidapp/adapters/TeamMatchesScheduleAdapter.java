package com.lfandroidapp.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.lfandroidapp.R;
import com.lfandroidapp.entities.Team;

import java.util.ArrayList;

/**
 * Created by vaio on 08/10/2015.
 */
public class TeamMatchesScheduleAdapter extends ArrayAdapter<ArrayList<Team>> {

    private final Activity context;
    private final ArrayList<ArrayList<Team>> teams;

    private ImageView imageView;
    private TextView textView;

    public TeamMatchesScheduleAdapter(Activity context, ArrayList<ArrayList<Team>> teams) {
        super(context, R.layout.list_team_matches_schedule, teams);
        this.context = context;
        this.teams = teams;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent){
        View rowView = view;

        LayoutInflater inflater = context.getLayoutInflater();

        ArrayList<Team> teamlist = teams.get(position);

        rowView = inflater.inflate(R.layout.list_team_matches_schedule, null, true);

        imageView = (ImageView)rowView.findViewById(R.id.icon_team_1);
        imageView.setImageResource(teamlist.get(0).getImage());

        textView = (TextView)rowView.findViewById(R.id.team_name_1);
        textView.setText(teamlist.get(0).getTeamName());

        imageView = (ImageView)rowView.findViewById(R.id.icon_team_2);
        imageView.setImageResource(teamlist.get(1).getImage());

        textView = (TextView)rowView.findViewById(R.id.team_name_2);
        textView.setText(teamlist.get(1).getTeamName());

        return rowView;
    }
}
