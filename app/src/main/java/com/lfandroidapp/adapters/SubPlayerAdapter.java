package com.lfandroidapp.adapters;

import android.app.Activity;
import android.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.lfandroidapp.MyTeamActivity;
import com.lfandroidapp.R;
import com.lfandroidapp.entities.Player;

import java.util.ArrayList;

/**
 * Created by vaio on 20/09/2015.
 */
public class SubPlayerAdapter extends ArrayAdapter<Player> {

    private final Activity context;
    private final ArrayList<Player> players;
    private Boolean invisibleBtnChange = true;
    private ImageView imageView;
    private TextView txtPlayerName;
    private Button btn;

    private Toast mToast;
    FragmentManager fm = null;


    public SubPlayerAdapter(Activity context, ArrayList<Player> items) {
        super(context, R.layout.list_subtitute_player, items);
        // TODO Auto-generated constructor stub

        this.context=context;
        this.players = items;
        fm = context.getFragmentManager();
    }

    public View getView(int position,View view,ViewGroup parent) {
        View rowView = view;
        final Player obj = players.get(position);
        LayoutInflater inflater=context.getLayoutInflater();
        rowView = inflater.inflate(R.layout.list_subtitute_player, null, true);
        imageView = (ImageView) rowView.findViewById(R.id.icon);
        txtPlayerName = (TextView) rowView.findViewById(R.id.playerName);
        btn = (Button) rowView.findViewById(R.id.btnChange);
        if(invisibleBtnChange) {
            btn.setVisibility(View.INVISIBLE);
        }else{
            btn.setVisibility(View.VISIBLE);
        }
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyTeamActivity callingActivity =(MyTeamActivity) context;
                String pos = callingActivity.replacePlayer(obj);
                callingActivity.invisibleChangeButtonInSubPlayerAdapter(true, pos);
            }
        });

        imageView.setImageResource(obj.getImage());
        txtPlayerName.setText(obj.getPlayerName());

        return rowView;

    };

    private void showToast(String message) {
        if (mToast != null) {
            mToast.cancel();
            mToast = null;
        }
        mToast = Toast.makeText(context, message, Toast.LENGTH_SHORT);
        mToast.show();
    }

    public Button getBtn() {
        return btn;
    }

    public void setBtn(Button btn) {
        this.btn = btn;
    }

    public Boolean getInvisibleBtnChange() {
        return invisibleBtnChange;
    }

    public void setInvisibleBtnChange(Boolean invisibleBtnChange) {
        this.invisibleBtnChange = invisibleBtnChange;
    }
}
