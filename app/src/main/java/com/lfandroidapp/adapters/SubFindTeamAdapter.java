package com.lfandroidapp.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.lfandroidapp.R;
import com.lfandroidapp.entities.Player;

import java.util.ArrayList;

/**
 * Created by vaio on 24/09/2015.
 */
public class SubFindTeamAdapter extends ArrayAdapter<Player>{

    private final Activity context;
    private final ArrayList<Player> players;

    private ImageView imageView;
    private TextView txtPlayerName;
    private Button btn;

    public SubFindTeamAdapter(Activity context, ArrayList<Player> items) {
        super(context, R.layout.list_team_mate, items);

        this.context = context;
        this.players = items;
    }

    public View getView(int position, View view, ViewGroup parent){
        View rowView = view;
        final Player player = players.get(position);
        LayoutInflater inflater = context.getLayoutInflater();
        rowView = inflater.inflate(R.layout.list_team_mate, null, true);

        imageView = (ImageView)rowView.findViewById(R.id.icon);
        txtPlayerName = (TextView)rowView.findViewById(R.id.playerName);
        btn = (Button) rowView.findViewById(R.id.btnCaptain);
        btn.setFocusable(false);
        if(player.getIsnew()){
            btn.setVisibility(View.INVISIBLE);
        }else {
            if (player.getIsCaptain()) {
                btn.setBackgroundResource(R.drawable.red_background);
            } else {
                btn.setBackgroundResource(R.drawable.white_background);
            }
        }

        imageView.setImageResource(player.getImage());
        txtPlayerName.setText(player.getPlayerName());

        return rowView;

    }
}
