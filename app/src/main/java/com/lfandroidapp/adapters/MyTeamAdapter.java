package com.lfandroidapp.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.lfandroidapp.R;
import com.lfandroidapp.entities.Team;

import java.util.ArrayList;

/**
 * Created by vaio on 22/09/2015.
 */
public class MyTeamAdapter extends ArrayAdapter<Team> {

    private final Activity context;
    private final ArrayList<Team> teamList;

    private ImageView imageView;
    private TextView txtTeamName;
    private Button btn;

    public MyTeamAdapter(Activity context, ArrayList<Team> items){
        super(context, R.layout.list_my_team, items);

        this.context = context;
        this.teamList = items;
    }

    public View getView(int position, View view ,ViewGroup parent){
        View rowView = view;
        final Team team = teamList.get(position);
        LayoutInflater inflater = context.getLayoutInflater();
        rowView = inflater.inflate(R.layout.list_my_team, null, true);

        imageView = (ImageView) rowView.findViewById(R.id.icon_team);
        txtTeamName = (TextView) rowView.findViewById(R.id.teamName);

        imageView.setImageResource(team.getImage());
        txtTeamName.setText(team.getTeamName());
        return rowView;
    }
}
