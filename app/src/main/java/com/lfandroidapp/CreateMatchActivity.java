package com.lfandroidapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.lfandroidapp.adapters.NothingSelectedSpinnerAdapter;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.Calendar;

public class CreateMatchActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener{

    private Spinner spinMyteam;
    private Spinner spinMyOpponent;
    private Spinner spinfs;
    private Spinner spinTime;
    private TextView txtDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_match);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_create_match);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        //build spinner
        String[] teams = {"ARSENAL","LIVERPOOL","MANCHESTER UNITED"};
        buildMyTeam(teams);

        buildMyOpponent(teams);

        String[] fslist = {"KUNINGAN", "SETIABUDI", "KARET"};
        buildFutsalStation(fslist);

        String[] times = {"11:00","12:00","13:00","14:00","20:00"};
        buildDate();
        buildAvailableTime(times);


    }

    private void buildDate(){
        txtDate = (TextView) findViewById(R.id.txtdate);
        txtDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = DatePickerDialog.newInstance(
                        CreateMatchActivity.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
                dpd.vibrate(true);
                dpd.dismissOnPause(true);
                dpd.setAccentColor(R.color.colorAccent);
                dpd.show(getFragmentManager(), "Datepickerdialog");
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        DatePickerDialog dpd = (DatePickerDialog) getFragmentManager().findFragmentByTag("Datepickerdialog");

        if(dpd != null) dpd.setOnDateSetListener(this);
    }


    private void buildMyTeam(String[] myteams){
        spinMyteam = (Spinner) findViewById(R.id.spinnerMyTeam);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.spinner_location, myteams);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinMyteam.setPrompt("My Team");
        spinMyteam.setAdapter(new NothingSelectedSpinnerAdapter(adapter, R.layout.spinner_my_team_nothing_selected, this));
    }

    private void buildMyOpponent(String[] myopponnents){
        spinMyOpponent = (Spinner) findViewById(R.id.spinnerOppTeam);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.spinner_location, myopponnents);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinMyOpponent.setPrompt("My Opponent");
        spinMyOpponent.setAdapter(new NothingSelectedSpinnerAdapter(adapter, R.layout.spinner_my_opponent_nothing_selected, this));
    }

    private void buildFutsalStation(String[] fsList){
        spinfs = (Spinner) findViewById(R.id.spinnerFutsalStation);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.spinner_location, fsList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinfs.setPrompt("Futsal Station");
        spinfs.setAdapter(new NothingSelectedSpinnerAdapter(adapter, R.layout.spinner_fs_nothing_selected, this));
    }

    private void buildAvailableTime(String[] times){
        spinTime = (Spinner) findViewById(R.id.spinnerTime);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.spinner_location, times);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinTime.setPrompt("Time");
        spinTime.setAdapter(new NothingSelectedSpinnerAdapter(adapter, R.layout.spinner_time_nothing_selected, this));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_create_match, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDateSet(DatePickerDialog datePickerDialog, int year, int monthOfYear, int dayOfMonth) {
        String date = dayOfMonth+"/"+(++monthOfYear)+"/"+year;
        txtDate.setText(date);
    }
}
